<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ProductModel;

class PublisherModel extends Model
{
    use HasFactory;
    protected $table = 'Publishers';

    public function books()
    {
        return $this->hasMany(ProductModel::class,'id','publisher_id');
    }
}