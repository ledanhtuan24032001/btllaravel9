<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ProductModel;
class ImageModel extends Model
{
    use HasFactory;
    protected $table = 'images';
    
   public function books()
   {
    return $this->hasOne(ProductModel::class,'book_id','id');
   }
}