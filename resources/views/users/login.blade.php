<!DOCTYPE html>
<html>

<!-- Mirrored from theme.nileforest.com/html/philos/login-register.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Sep 2022 17:25:19 GMT -->

<head>
    <meta charset="utf-8">
    <title>{{ $title }}</title>
    <meta name="description" content="Philos Template" />
    <meta name="keywords" content="philos, WooCommerce, bootstrap, html template, philos template">
    <meta name="author" content="philos" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

    <!-- Favicone Icon -->
    <link rel="shortcut icon" type="image/x-icon" href="users/img/favicon.ico">
    <link rel="icon" type="users/img/png" href="users/img/favicon.png">
    <link rel="apple-touch-icon" href="users/img/favicon.png">

    <!-- CSS -->
    <link href="users/css/plugins/bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap css -->
    <link href="users/css/plugins/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- fontawesome css -->
    <link href="users/css/plugins/animate.css" rel="stylesheet" type="text/css" />
    <!-- animate css -->
    <link href="users/css/style.css" rel="stylesheet" type="text/css" />
    <!-- template css -->
    <link href="users/users/plugins/rev_slider/css/settings-ver.5.3.1.css" rel="stylesheet" type="text/css" />
    <!-- Slider Revolution Css Setting -->

</head>

<body>

    <!-- Newsletter Popup ---------------------------------------------------->
    <section id="nlpopup" data-expires="30" data-delay="10">
        <!--Close Button-->
        <a href="javascript:void(0)" class="nlpopup_close nlpopup_close_icon">
            <img src="users/img/close-icon-white.png" alt="Newsletter Close" /></a>
        <!--End Close Button-->

        <h3 class="mb-40">Join Our Mailing List </h3>
        <p class="black mb-20">
            But I must explain to you how all this mistaken<br />
            idea of denouncing pleasure pain.
        </p>
        <form>
            <input class="input-md" name="footeremail" title="Enter Email Address.." placeholder="example@domain.com"
                type="email">
            <button class="btn btn-md btn-color">Subscribe</button>
        </form>
        <label class="mt-20">
            Sign up For Exclusive Updates, New Arrivals<br />
            And Insider-Only Discount.</label>
        <a class="nlpopup_close nlpopup_close_link mt-40">&#10006; Close</a>
    </section>
    <!-- Overlay -->
    <div id="nlpopup_overlay"></div>
    <!-- End Newsletter Popup ------------------------------------------------>


    <!-- Sidebar Menu (Cart Menu) ------------------------------------------------>
    <section id="sidebar-right" class="sidebar-menu sidebar-right">
        <div class="cart-sidebar-wrap">

            <!-- Cart Headiing -->
            <div class="cart-widget-heading">
                <h4>Shopping Cart</h4>
                <!-- Close Icon -->
                <a href="javascript:void(0)" id="sidebar_close_icon" class="close-icon-white"></a>
                <!-- End Close Icon -->
            </div>
            <!-- End Cart Headiing -->

            <!-- Cart Product Content -->
            <div class="cart-widget-content">
                <div class="cart-widget-product ">

                    <!-- Empty Cart -->
                    <div class="cart-empty">
                        <p>You have no items in your shopping cart.</p>
                    </div>
                    <!-- EndEmpty Cart -->

                    <!-- Cart Products -->
                    <ul class="cart-product-item">

                        <!-- Item -->
                        <li>
                            <!--Item Image-->
                            <a href="#" class="product-image">
                                <img src="users/img/product-img/small/product_12547554.jpg" alt="" /></a>

                            <!--Item Content-->
                            <div class="product-content">
                                <!-- Item Linkcollateral -->
                                <a class="product-link" href="#">Alpha Block Black Polo T-Shirt</a>

                                <!-- Item Cart Totle -->
                                <div class="cart-collateral">
                                    <span class="qty-cart">1</span>&nbsp;<span>&#215;</span>&nbsp;<span
                                        class="product-price-amount"><span class="currency-sign">$</span>399.00</span>
                                </div>

                                <!-- Item Remove Icon -->
                                <a class="product-remove" href="javascript:void(0)"><i class="fa fa-times-circle"
                                        aria-hidden="true"></i></a>
                            </div>
                        </li>

                        <!-- Item -->
                        <li>
                            <!--Item Image-->
                            <a href="#" class="product-image">
                                <img src="users/img/product-img/small/product_12547555.jpg" alt="" /></a>

                            <!--Item Content-->
                            <div class="product-content">
                                <!-- Item Linkcollateral -->
                                <a class="product-link" href="#">Red Printed Round Neck T-Shirt</a>

                                <!-- Item Cart Totle -->
                                <div class="cart-collateral">
                                    <span class="qty-cart">2</span>&nbsp;<span>&#215;</span>&nbsp;<span
                                        class="product-price-amount"><span class="currency-sign">$</span>299.00</span>
                                </div>

                                <!-- Item Remove Icon -->
                                <a class="product-remove" href="javascript:void(0)"><i class="fa fa-times-circle"
                                        aria-hidden="true"></i></a>
                            </div>
                        </li>

                    </ul>
                    <!-- End Cart Products -->

                </div>
            </div>
            <!-- End Cart Product Content -->

            <!-- Cart Footer -->
            <div class="cart-widget-footer">
                <div class="cart-footer-inner">

                    <!-- Cart Total -->
                    <h4 class="cart-total-hedding normal"><span>Total :</span><span
                            class="cart-total-price">$698.00</span></h4>
                    <!-- Cart Total -->

                    <!-- Cart Buttons -->
                    <div class="cart-action-buttons">
                        <a href="cart.html" class="view-cart btn btn-md btn-gray">View Cart</a>
                        <a href="checkout.html" class="checkout btn btn-md btn-color">Checkout</a>
                    </div>
                    <!-- End Cart Buttons -->

                </div>
            </div>
            <!-- Cart Footer -->
        </div>
    </section>
    <!--Overlay-->
    <div class="sidebar_overlay"></div>
    <!-- End Sidebar Menu (Cart Menu) -------------------------------------------->

    <!-- Search Overlay Menu ----------------------------------------------------->
    <section class="search-overlay-menu">
        <!-- Close Icon -->
        <a href="javascript:void(0)" class="search-overlay-close"></a>
        <!-- End Close Icon -->
        <div class="container">
            <!-- Search Form -->
            <form role="search" id="searchform" action="http://theme.nileforest.com/search" method="get">
                <div class="search-icon-lg">
                    <img src="users/img/search-icon-lg.png" alt="" />
                </div>
                <label class="h6 normal search-input-label" for="search-query">Enter keywords to Search
                    Product</label>
                <input value="" name="q" type="search" placeholder="Search..." />
                <button type="submit">
                    <img src="users/img/search-lg-go-icon.png" alt="" />
                </button>
            </form>
            <!-- End Search Form -->

        </div>
    </section>
    <!-- End Search Overlay Menu ------------------------------------------------>

    <!--==========================================-->
    <!-- wrapper -->
    <!--==========================================-->
    <div class="wraper">
        <!-- Header -->
        <header class="header">
            <!--Topbar-->
            @include('users.layout.header-topbar')
            <!--End Topbart-->

            <!-- Header Container -->
            @include('users.layout.header-container')
            <!-- End Header Container -->
        </header>
        <!-- End Header -->

        <!-- Page Content Wraper -->
        <div class="page-content-wraper">
            <!-- Bread Crumb -->
            <section class="breadcrumb">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <nav class="breadcrumb-link">
                                <a href="#">Home</a>
                                <span>Login & Register</span>
                            </nav>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Bread Crumb -->

            <!-- Page Content -->
            <section class="content-page">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-border-box">
                                <form action="{{ route('do.login') }}" method="POST">
                                    @csrf
                                    <h2 class="normal"><span>Registered Customers</span></h2>
                                    <p>Lorem ipsum dolor amet, conse adipiscing, eiusmod tempor incididunt ut labore et
                                        dolore magna aliqua.</p>
                                    <div class="form-field-wrapper">
                                        <label>Enter Your Email <span class="required">*</span></label>
                                        <input id="author-email" class="input-md form-full-width" name="email"
                                            placeholder="Enter Your Email Address" value="" size="30"
                                            aria-required="true" required="" type="email">
                                    </div>
                                    @if ($errors->has('email'))
                                        <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                    @endif

                                    <div class="form-field-wrapper">
                                        <label>Enter Your Password <span class="required">*</span></label>
                                        <input id="author-pass" class="input-md form-full-width" name="password"
                                            placeholder="Enter Your Password" value="" size="30"
                                            aria-required="true" required="" type="password">
                                    </div>

                                    @if ($errors->has('password'))
                                        <strong class="text-danger">{{ $errors->first('password') }}</strong>
                                    @endif
                                    <div class="form-field-wrapper">
                                        <input name="submit" id="submit" class="submit btn btn-md btn-black"
                                            value="Sign In" type="submit">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-border-box">
                                <form>
                                    <h2 class="normal"><span>New Customers</span></h2>
                                    <p>Lorem ipsum dolor amet, conse adipiscing, eiusmod tempor incididunt ut labore et
                                        dolore magna aliqua.</p>
                                    <div class="form-field-wrapper">
                                        <input name="submit" id="submit1" class="submit btn btn-md btn-color"
                                            value="Create An Account" type="submit">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Page Content -->

        </div>
        <!-- End Page Content Wraper -->

        <!-- Footer Section -------------->
        @include('users.layout.footer')
        <!-- End Footer Section -------------->

    </div>
    <!-- End wrapper =============================-->

    <!--==========================================-->
    <!-- JAVASCRIPT -->
    <!--==========================================-->
    <script type="text/javascript" src="users/js/jquery.min.js"></script>
    <script type="text/javascript" src="users/js/plugins/jquery-ui.js"></script>
    <!-- jquery library js -->
    <script type="text/javascript" src="users/js/plugins/modernizr.js"></script>
    <!--modernizr Js-->
    <script type="text/javascript" src="users/plugins/rev_slider/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="users/plugins/rev_slider/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="users/plugins/rev_slider/js/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="users/plugins/rev_slider/js/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="users/plugins/rev_slider/js/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="users/plugins/rev_slider/js/revolution.extension.layeranimation.min.js"></script>
    <!--Slider Revolution Js File-->
    <script type="text/javascript" src="users/js/plugins/tether.min.js"></script>
    <!--Bootstrap tooltips require Tether (Tether Js)-->
    <script type="text/javascript" src="users/js/plugins/bootstrap.min.js"></script>
    <!-- bootstrap js -->
    <script type="text/javascript" src="users/js/plugins/owl.carousel.js"></script>
    <!-- carousel js -->
    <script type="text/javascript" src="users/js/plugins/slick.js"></script>
    <!-- Slick Slider js -->
    <script type="text/javascript" src="users/js/plugins/plugins-all.js"></script>
    <!-- Plugins All js -->
    <script type="text/javascript" src="users/js/custom.js"></script>
    <!-- custom js -->
    <!-- end jquery -->

</body>

<!-- Mirrored from theme.nileforest.com/html/philos/login-register.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Sep 2022 17:25:19 GMT -->

</html>
