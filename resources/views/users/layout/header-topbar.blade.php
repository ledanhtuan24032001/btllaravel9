<div class="header-topbar">
    <div class="header-topbar-inner">
        <!--Topbar Left-->
        <div class="topbar-left hidden-sm-down">
            <div class="phone"><i class="fa fa-phone left" aria-hidden="true"></i>Customer Support : <b>+77 7565 348 576</b></div>
        </div>
        <!--End Topbar Left-->

        <!--Topbar Right-->
        <div class="topbar-right">
            <ul class="list-none">
                <li>
                    <a href="login-register.html"><i class="fa fa-lock left" aria-hidden="true"></i><span class="hidden-sm-down">Login</span></a>
                </li>
                <li class="dropdown-nav">
                    <a href="login-register.html"><i class="fa fa-user left" aria-hidden="true"></i><span class="hidden-sm-down">My Account</span><i class="fa fa-angle-down right" aria-hidden="true"></i></a>
                    <!--Dropdown-->
                    <div class="dropdown-menu">
                        <ul>
                            <li><a href="login-register.html">My Account</a></li>
                            <li><a href="#">Order History</a></li>
                            <li><a href="#">Returns</a></li>
                            <li><a href="#">My Wishlist</a></li>
                            <li><a href="checkout.html">Checkout</a></li>
                        </ul>
                        <span class="divider"></span>
                        <ul>
                            <li><a href="login-register.html"><i class="fa fa-lock left" aria-hidden="true"></i>Login</a></li>
                            <li><a href="login-register.html"><i class="fa fa-user left" aria-hidden="true"></i>Create an Account</a></li>
                        </ul>
                    </div>
                    <!--End Dropdown-->
                </li>
                <li class="dropdown-nav">
                    <a href="#">USD<i class="fa fa-angle-down right" aria-hidden="true"></i></a>
                    <!--Dropdown-->
                    <div class="dropdown-menu">
                        <ul>
                            <li><a href="#">USD</a></li>
                            <li><a href="#">EUR</a></li>
                            <li><a href="#">GBP</a></li>
                            <li><a href="#">AUD</a></li>
                        </ul>
                    </div>
                    <!--End Dropdown-->
                </li>
                <li>
                    <a href="about.html">About</a>
                </li>
                <li>
                    <a href="contact.html">Contact</a>
                </li>
            </ul>
        </div>
        <!-- End Topbar Right -->
    </div>
</div>